#!/usr/bin/env python3
#This file is part of JimPy.
#
#JimPy is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#JimPy is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with JimPy. If not, see <http://www.gnu.org/licenses/>.'''

from setuptools import setup, find_packages

setup(
    name = "JimPy",
    version = "0.0.1a3",
    author = "Ricardo Gomez",
    author_email = "ricardo.gomez331@gmail.com",
    description = "A simple library for interfacing with JIM",
    license = "GPLv3",
    keywords = "jim client",
    url = "https://jim.hackpad.com/")
