# JimPy

Copyright 2013 Ricardo Gomez

A python library to interface with the [JIM](http://jim.hackpad.com) protocol.

## Usage

### Client:

    from jimpy import client
    jim = client.open('127.0.0.1', 4004)
    client.authenticate(jim, user='rhg135', password='apass')
