'''This file is part of JimPy.

    JimPy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JimPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JimPy. If not, see <http://www.gnu.org/licenses/>.'''

import socket as _socket
import json
import threading
from collections import deque
import time

MAX_JSON =640
BUF_SIZE = 4096

def _open(socket, lock):
  '''Returns a new jim object on the socket.

  Arguments:
  socket -- the socket
  lock -- a lock to use for threading, if None will create one

  Return:
  a new jim object, you are not meant to know what is as it may change'''
  if not lock:
    lock = threading.Lock()

  read, unread, lock = pushback_socket(socket, lock)

  def write(data):
    with lock:
      sock.send(data)

  return {'read': read,
      'unread': unread,
      'write': write}
  
  def pushback_socket(socket, lock):
    '''Implements a function that can read and unread a socket'''
  buffer = deque([])
  def unread(data):
    '''Shoves data back in the buffer.'''
    with bufferlock:
      buffer.extendLeft(reversed(data))
    return data
  def read(amt=1):
    '''Reads a certain ammount from a socket.'''
    if len(buffer) < amt:
      with lock:
        data = socket.recv(BUF_SIZE)
      with bufferlock:
        buf.extend(data)
      if len(buf) >= amt:
        return bytes(buf.popleft() for i in range(amt))
  return (read, unread)

def open_socket(host, port):
  '''Opens a socket over ipv4 to `host` at `port`
  Returns a dictionary of the input, output and the socket'''
  sock = _socket.socket(_socket.AF_INET, _socket.SOCK_STREAM)
  sock.connect((host, port))
  sock.setblocking(0) #TODO: Make this an option
  return open(sock)

def _read(jim):
  '''Returns the next json object from the `jim`'''
  read = jim['read']
  unread = jim['unread']
  #TODO: this is an ugly way to express the idea
  i = MAX_JSON
  s = read(i)
  while not s:
    if i < 2:
      return
    s = read(i)
    i -= 1
  data = s.decode('UTF-8')
  while len(data) > 0:
    print(data)
    try:
      return json.loads(data)
    except Exception as e:
      unread(data[-1:])
      data = data[:-1]

def read(jim, block=True):
  '''Reads the next json object from the jim.

  Arguments:
  jim -- the user
  block -- A boolean indicating if we should block

  Return:
  A dictionary'''
  if block:
    o = None
    while not o:
      o = _read(jim)
    return o
  else:
    return _read(jim)

def write(jim, obj):
	'''writes an object as json to the `jim`'''
	jim['write'](json.dumps(obj).encode('UTF-8'))

if __name__ == '__main__':
	jim = open_socket('127.0.0.1', 4004)
	print(read(jim))
	write(jim, {'action': 'quit'})
